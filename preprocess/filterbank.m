%% The filters were getting from:
%%
%% http://www.robots.ox.ac.uk/~vgg/research/texclass/filters.html

pkg load image

arg_list = argv();

dir_file = arg_list{1};
file_name = arg_list{2};
ext_file = arg_list{3};
name_file_out = arg_list{4};

file_path = strcat(dir_file,"/",file_name,".",ext_file);

I = imread(file_path);
FLM = makeLMfilters();


for i=1:48
  imwrite(imfilter(I, FLM(:,:,i)),
	  strcat(dir_file,
		 "/",
		 name_file_out,
		 "_flm",
		 sprintf("%03d",i),
		 ".", ext_file));
endfor;

FS = makeSfilters();

for i=1:13
  imwrite(imfilter(I, FS(:,:,i)),
	  strcat(dir_file,
		 "/",
		 name_file_out,
		 "_fs",
		 sprintf("%03d",i),
		 ".", ext_file));
endfor;

FRFS = makeRFSfilters();

for i=1:38
  imwrite(imfilter(I, FRFS(:,:,i)),
	  strcat(dir_file,
		 "/",
		 name_file_out,
		 "_frfs",
		 sprintf("%03d",i),
		 ".", ext_file));
endfor;