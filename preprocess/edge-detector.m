pkg load image

arg_list = argv();

file_name = arg_list{1};
output = arg_list{2};
I = imread(file_name);

imwrite(edge(I, "Canny"), output);
disp("Finish edge detection");
